fetch('http://jsonplaceholder.typicode.com/todos/1')
.then((response)=>response.json())
.then((json)=>console.log(json));

//  Getting all to do list ITEM
fetch('http://jsonplaceholder.typicode.com/todos')
.then((response)=>response.json())
.then((json)=>{

let list = json.map((todo=>{
	return todo.title
}))
console.log(list)

});

// // getting a specific to do list item
fetch('http://jsonplaceholder.typicode.com/todos/1')
.then((response)=>response.json())
.then((json)=>console.log(`the item "${json.title}" on the list has a status of ${json.completed}`));

// create a todo list item using post method
fetch('http://jsonplaceholder.typicode.com/todos',{
	method: 'POST',
	headers:{
		'Content-Type': 'application/json'
	},
	body:JSON.stringify({
		title: 'Created to Do list ITem',
		completed: false,
		userId: 1
	})
})
.then((response)=>response.json())
.then((json)=>console.log(json));

// Update a to do list item using PUT method

fetch('http://jsonplaceholder.typicode.com/todos/1',{
	method :'PUT'
	headers:{
		'Content-Type':'application/json'
	},
	body:JSON.stringify({
		title: "Updated to Do list Item",
		description: "To update the my to do list with different data structure.",
		status: "Pending",
		dateCompleted: 'Pending'
		userId:1

	})
})
.then((response)=>response.json())
.then((json)=>console.log(json));

// updating a to do list item using patch method

fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: "PATCH",
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			status: "Completed",
			dateCompleted: "07/09/21"
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));


// delete a to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'DELETE',

})