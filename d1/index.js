// https://jsonplaceholder.typicode.com/posts

// JAVASCRIPT Synchronous and aSynchronous
/*
Synchronous
	-Synchronous code runs in sequence. this means that each operation must wait for the previous one to complete before executing

Asynchronous
	-aSynchronous code runs in parallel. this means that an operation can occur while another one is still being processed.

	-aSynchronous code execution is ofer prefferrable in situations wher executiuon can be block . Some examples of this are network requests, long running calculation, file system operatoin, etc. using aSynchronous code in the browser ensures the page remains response and the user experience is mostly unaffected
*/
// // EXampel
// console.log('hello world')
// for (let i = 0; i<=1000; i++){
// 	console.log(i)
// }
// console.log('hello why')
/*
	-API stand for Application Programming Interface
	-An application programming interface is a particular set of codes that allows software programs to communicate w/ each other
	-an API is the interface through w/c you access someone else's code or through w/c someone else's code access yours

	Example: 
		Google APIs

		Youtube API
			-https://developers.google.com/youtube/iframe_api_reference

*/

// FETCH API
	// console.log(fetch('https://jsonplaceholder.typicode.com/posts'))
/*
	- A "promise" is an Object that represent the eventual completion or failure of an asynchronous function and its resulting value

	-a promise is in one of these three states:
		pending : initial state, neither fulfilled nor rejected
		Fulffiled: Operation was succesfull completed
		rejected: operation failed
*/	
/*
	-By using the .then method, we can now check for the status of the promise
	-the "fetch" method will return a "promise" that resolves to be a "response" object
	-the ".then" method captures the "response" object and returns another "promise" w/ will eventually be "resolved" or "rejected"	
		-syntax
		fetch('URL')
		.then((response) => {})
*/
// fetch('https://jsonplaceholder.typicode.com/posts')
// 	.then(response => console.log(response.status) )

// fetch('https://jsonplaceholder.typicode.com/posts')
// // use the "json" method form the "response" object to convert the data retrieve into JSON format to be use in our application
// .then(response => response.json())
// // print the converted JSON bvalue form the "Fetch" request
// // using multiple ".then" method to create a promise chain
// .then((json)=>console.log(json))

/*
	-the "async" and "await" keywords is another approach that can be used to achieve asynchronous codes
	-used in function to indicate w/c portion of code should ne waited
	-creates
	 asynchronous function
*/

// async function fetchData(){
// 	//waits for the fetch method to complate then stores the value in the "result" variable"
// 	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
// 	// Result returned by fetch is returned as a promise
// 	console.log(result)
// 	// the returned "response" is an object
// 	console.log(typeof result);
// 	// we connot acces the content of the "response" by directly accessing its body property
// 	console.log(result.body)
// 	let json = await result.json();
// 	console.log(json)
// };
// fetchData();


// // GETTING A SPECIFIC POST
// // retrieves a specific post ff the REST API (Retrieve, /posts/id:,GET)

// fetch('https://jsonplaceholder.typicode.com/posts/1')
// .then((response) =>response.json())
// .then((json) => console.log(json));

/*
	Postman
	Urt:https://jsonplaceholder.typicode.com/posts/1
	method: GET
	
*/
/*
	CREATING A POST
		syntax:
		fetch('URL',options)
		.then((response)=>{})
		.then((response)=>{})
*/

// Creates a new post ff the REST API (Create,/post/:id, POST)
// fetch('https://jsonplaceholder.typicode.com/posts',{
// method: 'POST',
// hearders:{
// 	'Content-Type': 'application/json'
// },
// body: JSON.stringify({
// 	title: 'New Post',
// 	body: 'Hello World',
// 	userId: 1

// 	})
// })
// // .then((response)=>response.json())
// // .then((json)=>console.log(json))

/*
	UPDATING A POST
		-updates a specific post ff the REST API (update,/post/:id,PUT)
*/
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'PUT',
	headers:{
		'Content-Type':'application/json'
	},
	body:JSON.stringify({
		id:1,
		title: 'updated post',
		body: 'Hello again',
		userId: 1
	})
})
.then((response)=>response.json())
.then((json)=>console.log(json))


// update apost using patch method
/*
	-updates a specific post following the rest API (update,/posts/:id,patch)
	-

*/

fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method : 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body:JSON.stringify({
		title: 'Corrected post'
	})
})
.then((response)=>response.json())
.then((json)=>console.log(json))


// Deleting a post
/*
	deleting a specific post ff the REST API (delete,/post/:id, DELETE)
*/
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'DELETE',

})

// filtering the post
/*
	-the data can be filtere by sending the USerID along w/ the URL
	-information send via TH url be done by adding the question mark
	symbol (?)
	syntax:
	individual parameters"
	'url?parameterName=value'
	multiple parameters
		'url?Parama=valueA&Parama=valueB'

*/
// fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
// .then((response)=>response.json())
// .then((json)=>console.log(json));


fetch('https://jsonplaceholder.typicode.com/posts?userId=1&userId=2&userId=3')
.then((response)=>response.json())
.then((json)=>console.log(json));
